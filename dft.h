#pragma once
#include <complex>
#include <vector>

std::vector<std::complex<double> > dft(const std::vector<double> &x);
std::vector<double> idft(const std::vector<std::complex<double> > &X);
