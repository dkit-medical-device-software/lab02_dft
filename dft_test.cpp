#include "gtest/gtest.h"
#include <vector>
#include "dft.h"

using namespace std;

// floating point types will rarely be exactly equal
const double tolerence = 1e-10;

TEST(DFT, ResultHasSameSizeAsInput) {
  vector<double> x;
  x.push_back(23);
  x.push_back(4);
  x.push_back(41);
  x.push_back(-19);
  vector<complex<double> > result = dft(x);
  ASSERT_EQ(x.size(), result.size());
}

TEST(DFT, SimpleDCCaseWorks) {
  vector<double> x;
  for ( int k = 0 ; k < 3; ++k ) {
    x.push_back(3);
  }
  vector<complex<double> > X = dft(x);
  ASSERT_DOUBLE_EQ(9, X[0].real());
  ASSERT_DOUBLE_EQ(0, X[0].imag());
}

TEST(DFT, SampleSequence1) {
  vector<double> x;
  x.push_back(3);
  x.push_back(-1);
  x.push_back(3);
  x.push_back(-1);
  vector<complex<double> > X = dft(x);
  double real[] = { 4, 0, 8, 0 };
  double imag[] = { 0, 0, 0, 0 };
  for ( int k = 0 ; k < x.size() ; ++k ) {
    EXPECT_NEAR(real[k], X[k].real(), tolerence);
    EXPECT_NEAR(imag[k], X[k].imag(), tolerence);
  }
}

TEST(DFT, SampleSequence2) {
  vector<double> x;
  x.push_back(2);
  x.push_back(5);
  x.push_back(2);
  x.push_back(1);
  vector<complex<double> > X = dft(x);
  double real[] = { 10, 0, -2, 0 };
  double imag[] = { 0, -4, 0, 4 };
  for ( int k = 0 ; k < x.size() ; ++k ) {
    EXPECT_NEAR(real[k], X[k].real(), tolerence) << " k=" << k;
    EXPECT_NEAR(imag[k], X[k].imag(), tolerence) << " k=" << k;
  }  
}

TEST(IDFT, ResultHasSameSizeAsInput) {
  vector<complex<double> > X;
  X.push_back(23);
  X.push_back(4);
  X.push_back(41);
  X.push_back(-19);
  vector<double> result = idft(X);
  ASSERT_EQ(X.size(), result.size());
}

TEST(IDFT, SampleSequenceDC) {
  vector<complex<double> > X;
  X.push_back(6);
  X.push_back(0);
  X.push_back(0);
  X.push_back(0);
  vector<double> result = idft(X);
  for ( int k = 0 ; k < X.size() ; ++k ) {
    EXPECT_NEAR(1.5, result[k], tolerence) << " k=" << k;
  }  
  
}
